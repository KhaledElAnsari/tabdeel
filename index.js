var tabdeel = (function() {
    function t(m) {
        var indianDigits = [
            '٠', // Zero
            '١', // One
            '٢', // Two
            '٣', // Three
            '٤', // Four
            '٥', // Five
            '٦', // Six
            '٧', // Seven
            '٨', // Eight
            '٩', // Nine
        ];
        var arabicDigits = [
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
        ];
        return m.replace(/[٠-۹]/g, function(c) {
            return arabicDigits[indianDigits.indexOf(c)];
        });
    }

    return t;
})();

module.exports = tabdeel;