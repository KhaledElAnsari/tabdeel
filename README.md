# Tabdeel

A small node module for replacing Arabic numbers with their equivalent in English

Here's a working fiddle for Tabdeel in action: https://jsfiddle.net/khaledelansari/8afjqn14/3/

# License

This module is under the MIT License