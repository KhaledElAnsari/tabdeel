var assert = require("assert");
var tabdeel = require("./index.js");

describe("Tabdeel", function() {
    it("Should replace characters", function() {
        assert.equal("0786115665", tabdeel("٠٧٨٦١١٥٦٦٥"));
    });
});